package com.example.progweb2020.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progweb2020.Model.DefaultResult;
import com.example.progweb2020.Model.Dosen;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;
import com.example.progweb2020.UTS.DosenViewActivity;
import com.example.progweb2020.UTS.DsnUpdateActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DsnAdapterUts extends RecyclerView.Adapter<DsnAdapterUts.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;
    ProgressDialog pd;

    public DsnAdapterUts(Context context){
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DsnAdapterUts(List<Dosen>dosenList){
        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList(){
        return dosenList;
    }

    public void setDosenList(List<Dosen>dosenList){
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_dsn,parent,false);
        return new ViewHolder(v,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);
        holder.tvNamaDsn.setText(d.getNama());
        holder.tvEmailDsn.setText(d.getEmail());
        holder.dsn = d;
        pd = new ProgressDialog(holder.context2);
        holder.btnDsnVUD.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final Dosen d = dosenList.get(position);
                PopupMenu popupMenu = new
                        PopupMenu(holder.context2,holder.btnDsnVUD);
                popupMenu.inflate(R.menu.menu_edit);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        switch (item.getItemId()){
                            case R.id.tbDel:
                                pd.setTitle("Deleting");
                                pd.show();
                                Call<DefaultResult> del = service.delete_dsn(d.getNidn().toString(),"72180182");
                                del.enqueue(new Callback<DefaultResult>() {
                                    @Override
                                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context2,"Deleted",Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context2,"Failed",Toast.LENGTH_LONG).show();
                                    }
                                });
                                break;
                            case R.id.tbView:
                                Intent dsnVw = new Intent(holder.context2, DosenViewActivity.class);
                                dsnVw.putExtra("nidn",d.getNidn());
                                dsnVw.putExtra("nama",d.getNama());
                                dsnVw.putExtra("email",d.getEmail());
                                dsnVw.putExtra("alamat",d.getAlamat());
                                dsnVw.putExtra("gelar",d.getGelar());
                                holder.context2.startActivity(dsnVw);
                                break;
                            case R.id.tbUpdate:
                                Intent dsnUp = new Intent(holder.context2, DsnUpdateActivity.class);
                                dsnUp.putExtra("nidn_cari",d.getNidn_cari());
                                dsnUp.putExtra("nama",d.getNama());
                                dsnUp.putExtra("nidn",d.getNidn());
                                dsnUp.putExtra("email",d.getEmail());
                                dsnUp.putExtra("alamat",d.getAlamat());
                                dsnUp.putExtra("gelar",d.getGelar());
                                holder.context2.startActivity(dsnUp);
                        }
                        return false;
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNamaDsn, tvEmailDsn, tvNoTelp;
        private ImageView btnDsnVUD;
        Context context2;
        Dosen dsn;
        public ViewHolder(@NonNull View itemView, Context context){
            super(itemView);
            tvNamaDsn = itemView.findViewById(R.id.tvNamaDsnList);
            tvEmailDsn = itemView.findViewById(R.id.tvEmailDsnList);
            btnDsnVUD = itemView.findViewById(R.id.btnDsnVUD);
            context2 = context;
        }
    }
}
