package com.example.progweb2020.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progweb2020.Model.DefaultResult;
import com.example.progweb2020.Model.Mahasiswa;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;
import com.example.progweb2020.UTS.DosenViewActivity;
import com.example.progweb2020.UTS.DsnUpdateActivity;
import com.example.progweb2020.UTS.MhsUpdateActivity;
import com.example.progweb2020.UTS.MhsViewActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class MhsAdapterUts extends RecyclerView.Adapter<MhsAdapterUts.ViewHolder> {
    private Context context;
    private List<Mahasiswa>mahasiswaList;
    ProgressDialog pd;

    public MhsAdapterUts(Context context){
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    public MhsAdapterUts(List<Mahasiswa>mahasiswaList){
        this.mahasiswaList = mahasiswaList;
    }

    public List<Mahasiswa>getMahasiswaList(){
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa>mahasiswaList){
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MhsAdapterUts.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_mhs,parent,false);
        return new ViewHolder(v,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull MhsAdapterUts.ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);
        holder.tvNamaMhs.setText(m.getNama());
        holder.tvEmailMhs.setText(m.getEmail());
        holder.mhs = m;
        pd = new ProgressDialog(holder.context1);
        holder.btnMhsVUD.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                PopupMenu popupMenu = new
                        PopupMenu(holder.context1,holder.btnMhsVUD);
                popupMenu.inflate(R.menu.menu_edit);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        switch (item.getItemId()){
                            case R.id.tbDel:
                                pd.setTitle("Deleting");
                                pd.show();
                                retrofit2.Call<DefaultResult> del = service.delete_mhs(m.getNim().toString(),"72180182");
                                del.enqueue(new Callback<DefaultResult>() {
                                    @Override
                                    public void onResponse(retrofit2.Call<DefaultResult> call, Response<DefaultResult> response) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context1, "Deleted", Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onFailure(retrofit2.Call<DefaultResult> call, Throwable t) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context1, "Failed", Toast.LENGTH_LONG).show();
                                    }
                                });
                                break;
                            case R.id.tbView:
                                Intent dsnVw = new Intent(holder.context1, MhsViewActivity.class);
                                dsnVw.putExtra("nim",m.getNim());
                                dsnVw.putExtra("nama",m.getNama());
                                dsnVw.putExtra("email",m.getEmail());
                                dsnVw.putExtra("alamat",m.getAlamat());
                                holder.context1.startActivity(dsnVw);
                                break;
                            case R.id.tbUpdate:
                                Intent dsnUp = new Intent(holder.context1, MhsUpdateActivity.class);
                                dsnUp.putExtra("nama",m.getNama());
                                dsnUp.putExtra("nim",m.getNim());
                                dsnUp.putExtra("email",m.getEmail());
                                dsnUp.putExtra("alamat",m.getAlamat());
                                holder.context1.startActivity(dsnUp);
                        }
                        return false;
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaMhs, tvEmailMhs;
        private ImageView btnMhsVUD;
        Context context1;
        Mahasiswa mhs;

        public ViewHolder(@NonNull View itemView,Context context){
            super(itemView);
            tvNamaMhs = itemView.findViewById(R.id.tvNamaMhsList);
            tvEmailMhs = itemView.findViewById(R.id.tvEmailMhsList);
            btnMhsVUD = itemView.findViewById(R.id.btnMhsVUD);
            context1 = context;
        }
    }
}