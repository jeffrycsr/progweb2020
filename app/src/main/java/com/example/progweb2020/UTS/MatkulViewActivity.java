package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.progweb2020.R;

public class MatkulViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_view);

        TextView tvKode = (TextView)findViewById(R.id.tvKodeMatkulView);
        TextView tvNama = (TextView)findViewById(R.id.tvNamaMatkulView);
        TextView tvHari = (TextView)findViewById(R.id.tvHariMatkulView);
        TextView tvSesi = (TextView)findViewById(R.id.tvSesiMatkulView);
        TextView tvSks  = (TextView)findViewById(R.id.tvSksMatkulView);

        Intent data = getIntent();
        if(data != null){
            tvKode.setText(data.getStringExtra("kode"));
            tvNama.setText(data.getStringExtra("nama"));
            tvHari.setText(data.getStringExtra("hari"));
            tvSesi.setText(data.getStringExtra("sesi"));
            tvSks.setText(data.getStringExtra("sks"));
        }
    }
}