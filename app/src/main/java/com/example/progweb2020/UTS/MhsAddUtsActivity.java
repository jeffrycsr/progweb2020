package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progweb2020.Model.DefaultResult;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MhsAddUtsActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mhs_add_uts);

        final EditText edAddNim = (EditText)findViewById(R.id.txtAddNimMhsUts);
        final EditText edAddNama = (EditText)findViewById(R.id.txtAddNamaMhsUts);
        final EditText edAddEmail = (EditText)findViewById(R.id.txtAddEmailMhsUts);
        final EditText edAddAlamat = (EditText)findViewById(R.id.txtAddAlamatMhsUts);
        Button btnSave = (Button)findViewById(R.id.btnSave);
        pd = new ProgressDialog(MhsAddUtsActivity.this);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        edAddNim.getText().toString(),
                        edAddNama.getText().toString(),
                        edAddEmail.getText().toString(),
                        edAddAlamat.getText().toString(),
                        "Kosong",
                        "72180182"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MhsAddUtsActivity.this,"Data Saved",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MhsAddUtsActivity.this,"Save Failed",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}