package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progweb2020.Model.DefaultResult;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DsnAddUtsActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dsn_add_uts);

        final EditText edNidn = (EditText)findViewById(R.id.editNidnDsn);
        final EditText edNamaDsn = (EditText)findViewById(R.id.editNamaDsn);
        final EditText edEmailDsn = (EditText)findViewById(R.id.editEmailDsn);
        final EditText edAlamatDsn = (EditText)findViewById(R.id.editAlamatDsn);
        final EditText edGelarDsn = (EditText)findViewById(R.id.editGelarDsn);
        Button btnSave = (Button)findViewById(R.id.btnSaveDsn);
        pd = new ProgressDialog(DsnAddUtsActivity.this);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dsn(
                        edNamaDsn.getText().toString(),
                        edNidn.getText().toString(),
                        edEmailDsn.getText().toString(),
                        edAlamatDsn.getText().toString(),
                        edGelarDsn.getText().toString(),
                        "Kosong",
                        "72180182"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DsnAddUtsActivity.this,"Data Saved",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DsnAddUtsActivity.this,"Failed",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}