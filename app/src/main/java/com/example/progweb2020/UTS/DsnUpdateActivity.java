package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextPaint;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progweb2020.Model.DefaultResult;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DsnUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dsn_update);

        final EditText edNidn = (EditText)findViewById(R.id.txtEditNidn);
        final EditText edUpNamaDsn = (EditText)findViewById(R.id.txtUpNamaDsn);
        final EditText edUpNidn = (EditText)findViewById(R.id.txtUpNidnDsn);
        final EditText edUpEmailDsn = (EditText)findViewById(R.id.txtUpEmailDsn);
        final EditText edUpAlamatDsn = (EditText)findViewById(R.id.txtUpAlamatDsn);
        final EditText edUpGelarDsn = (EditText)findViewById(R.id.txtUpGelarDsn);
        Button btnUpdateDsn = (Button)findViewById(R.id.btnUpdateDsn);
        pd = new ProgressDialog(DsnUpdateActivity.this);

        Intent data = getIntent();
        if(data!=null){
            edNidn.setText(data.getStringExtra("nidn"));
            edUpNamaDsn.setText(data.getStringExtra("nama"));
            edUpNidn.setText(data.getStringExtra("nidn_cari"));
            edUpEmailDsn.setText(data.getStringExtra("email"));
            edUpAlamatDsn.setText(data.getStringExtra("alamat"));
            edUpGelarDsn.setText(data.getStringExtra("gelar"));

            btnUpdateDsn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Mohon menunggu");
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> up = service.delete_dsn(
                            edNidn.getText().toString(),
                            "72180182"
                    );
                    up.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            Toast.makeText(DsnUpdateActivity.this,"Update Success",Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(DsnUpdateActivity.this,"Failed",Toast.LENGTH_LONG);
                        }
                    });

                    Call<DefaultResult> adddsn = service.add_dsn(
                            edUpNamaDsn.getText().toString(),
                            edUpNidn.getText().toString(),
                            edUpEmailDsn.getText().toString(),
                            edUpAlamatDsn.getText().toString(),
                            edUpGelarDsn.getText().toString(),
                            "Kosongkan",
                            "72180182"
                    );

                    adddsn.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(DsnUpdateActivity.this,"Update Success", Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(DsnUpdateActivity.this,"Failed",Toast.LENGTH_LONG);
                        }
                    });
                }
            });
        }
    }
}