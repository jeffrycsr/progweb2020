package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progweb2020.Model.DefaultResult;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MhsUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mhs_update);

        final EditText txtCariNim = (EditText) findViewById(R.id.txtCariNimMhs);
        final EditText txtUpNama = (EditText) findViewById(R.id.txtUpNamaMhs);
        final EditText txtUpNim = (EditText) findViewById(R.id.txtUpNimMhs);
        final EditText txtUpEmail = (EditText) findViewById(R.id.txtUpEmailMhs);
        final EditText txtUpAlamat = (EditText) findViewById(R.id.txtUpAlamatMhs);
        Button btnUpdateMhs = (Button) findViewById(R.id.btnUpdateMhs);
        pd = new ProgressDialog(MhsUpdateActivity.this);

        Intent data = getIntent();
        if (data != null) {
            txtCariNim.setText(data.getStringExtra("nim"));
            txtUpNama.setText(data.getStringExtra("nama"));
            txtUpNim.setText(data.getStringExtra("nim_update"));
            txtUpEmail.setText(data.getStringExtra("email"));
            txtUpAlamat.setText(data.getStringExtra("alamat"));

            btnUpdateMhs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Mohon Menunggu");
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> up = service.delete_mhs(
                            txtCariNim.getText().toString(),
                            "72180182"
                    );
                    up.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            Toast.makeText(MhsUpdateActivity.this,"Update Succes",Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MhsUpdateActivity.this,"Failed",Toast.LENGTH_LONG);
                        }
                    });

                    Call<DefaultResult> addmhs = service.add_mhs(
                            txtUpNim.getText().toString(),
                            txtUpNama.getText().toString(),
                            txtUpEmail.getText().toString(),
                            txtUpAlamat.getText().toString(),
                            "Kosongkan",
                            "72180182"
                    );

                    addmhs.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(MhsUpdateActivity.this,"Update Success",Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MhsUpdateActivity.this,"Failed",Toast.LENGTH_LONG);
                        }
                    });
                }
            });
        }
    }
}