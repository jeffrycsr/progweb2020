package com.example.progweb2020.UTS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.progweb2020.Adapter.MatkulAdapterUts;
import com.example.progweb2020.Adapter.MhsAdapterUts;
import com.example.progweb2020.Model.Mahasiswa;
import com.example.progweb2020.Model.Matkul;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulUtsActivity extends AppCompatActivity {
    RecyclerView rvMatkulUts;
    MatkulAdapterUts matkulAdapterUts;
    ProgressDialog pd;
    List<Matkul>matkulList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_uts);

        rvMatkulUts = (RecyclerView)findViewById(R.id.rvMatkulUts);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon menunggu");
        pd.show();

        androidx.appcompat.widget.Toolbar tbMatkulUts = (androidx.appcompat.widget.Toolbar) findViewById(R.id.tbMatkulUts);
        setSupportActionBar(tbMatkulUts);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matkul>> call = service.getMatkul("72180182");

        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss();
                matkulList = response.body();
                matkulAdapterUts = new MatkulAdapterUts(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MatkulUtsActivity.this);
                rvMatkulUts.setLayoutManager(layoutManager);
                rvMatkulUts.setAdapter(matkulAdapterUts);
            }

            @Override
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                Toast.makeText(MatkulUtsActivity.this,"Error",Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add,menu);
        return  super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tbAdd:
                Intent intent = new Intent(MatkulUtsActivity.this,MatkulAddUtsActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}