package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.progweb2020.R;

public class DosenViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_view);

        TextView tvNidn = (TextView)findViewById(R.id.tvNidnDsn);
        TextView tvNamaDsn = (TextView)findViewById(R.id.tvNamaDsnList);
        TextView tvEmailDsn = (TextView)findViewById(R.id.tvEmailDsnList);
        TextView tvAlamatDsn = (TextView)findViewById(R.id.tvAlamatDsn);
        TextView tvGelarDsn = (TextView)findViewById(R.id.tvGelarDsn);

        Intent data = getIntent();
        if(data != null){
            tvNidn.setText(data.getStringExtra("nidn"));
            tvNamaDsn.setText(data.getStringExtra("nama"));
            tvEmailDsn.setText(data.getStringExtra("email"));
            tvAlamatDsn.setText(data.getStringExtra("alamat"));
            tvGelarDsn.setText(data.getStringExtra("gelar"));
        }
    }
}