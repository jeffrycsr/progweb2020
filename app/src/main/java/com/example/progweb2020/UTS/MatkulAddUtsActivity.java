package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progweb2020.Model.DefaultResult;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulAddUtsActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_add_uts);

        final EditText edAddKode = (EditText)findViewById(R.id.txtAddKodeMatkulUts);
        final EditText edAddNama = (EditText)findViewById(R.id.txtAddNamaMatkulUts);
        final EditText edAddHari = (EditText)findViewById(R.id.txtAddHariMatkulUts);
        final EditText edAddSesi = (EditText)findViewById(R.id.txtAddSesiMatkulUts);
        final EditText edAddSks = (EditText)findViewById(R.id.txtAddSksMatkulUts);
        Button btnSave = (Button)findViewById(R.id.btnSave2);
        pd = new ProgressDialog(MatkulAddUtsActivity.this);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        edAddNama.getText().toString(),
                        "72180182",
                        edAddKode.getText().toString(),
                        edAddHari.getText().toString(),
                        edAddSesi.getText().toString(),
                        edAddSks.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddUtsActivity.this,"Data Saved",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddUtsActivity.this,"Failed",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}