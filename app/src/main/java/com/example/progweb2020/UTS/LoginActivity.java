package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    ProgressDialog pd;
    String isLogin="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        EditText nim = (EditText)findViewById(R.id.txtNimUts);
        EditText pass = (EditText)findViewById(R.id.txtPassUts);
        Button btnLogin = (Button)findViewById(R.id.btnLoginUts);
        pd=new ProgressDialog(LoginActivity.this);

        SharedPreferences pref = LoginActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        isLogin = pref.getString("isLogin","0");
        if(isLogin.equals("1")){
            Intent intent = new Intent(LoginActivity.this, HomeMenuActivity.class);
            startActivity(intent);
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Log In");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<ModelLogin>> login = service.Login(
                        nim.getText().toString(),
                        pass.getText().toString()
                );
                login.enqueue(new Callback<List<ModelLogin>>() {
                    @Override
                    public void onResponse(Call<List<ModelLogin>> call, Response<List<ModelLogin>> response) {
                        if(response.body().size()!=0){
                            editor.putString("isLogin","1");
                            editor.commit();
                            pd.dismiss();
                            Intent intent = new Intent(LoginActivity.this, HomeMenuActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            Toast.makeText(LoginActivity.this,"Success",Toast.LENGTH_LONG).show();
                        }else{
                            pd.dismiss();
                            Toast.makeText(LoginActivity.this,"Failed",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ModelLogin>> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(LoginActivity.this,"Failed",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}