package com.example.progweb2020.UTS;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.progweb2020.Adapter.MhsAdapterUts;
import com.example.progweb2020.Model.Mahasiswa;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MhsUtsActivity extends AppCompatActivity {
    RecyclerView rvMhsUts;
    MhsAdapterUts mhsAdapterUts;
    ProgressDialog pd;
    List<Mahasiswa>mahasiswaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mhs_uts);

        rvMhsUts = (RecyclerView)findViewById(R.id.rvMhsUts);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon menunggu");
        pd.show();

        androidx.appcompat.widget.Toolbar tbMhsUts = (androidx.appcompat.widget.Toolbar) findViewById(R.id.tbMhsUts);
        setSupportActionBar(tbMhsUts);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Mahasiswa>> call = service.getMahasiswa("72180182");

        call.enqueue(new Callback<List<Mahasiswa>>() {
            @Override
            public void onResponse(Call<List<Mahasiswa>> call, Response<List<Mahasiswa>> response) {
                pd.dismiss();
                mahasiswaList = response.body();
                mhsAdapterUts = new MhsAdapterUts(mahasiswaList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MhsUtsActivity.this);
                rvMhsUts.setLayoutManager(layoutManager);
                rvMhsUts.setAdapter(mhsAdapterUts);
            }

            @Override
            public void onFailure(Call<List<Mahasiswa>> call, Throwable t) {
                Toast.makeText(MhsUtsActivity.this,"Error",Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_add,menu);
            return super.onCreateOptionsMenu(menu);
        }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tbAdd:
                Intent intent = new Intent(MhsUtsActivity.this,MhsAddUtsActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}