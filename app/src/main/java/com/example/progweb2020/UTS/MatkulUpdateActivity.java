package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progweb2020.Model.DefaultResult;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        final EditText txtCariKode = (EditText)findViewById(R.id.txtCariKodeMatkul);
        final EditText txtUpNama = (EditText)findViewById(R.id.txtUpNamaMatkul);
        final EditText txtUpKode = (EditText)findViewById(R.id.txtUpKodeMatkul);
        final EditText txtUpHari = (EditText)findViewById(R.id.txtUpHariMatkul);
        final EditText txtUpSesi = (EditText)findViewById(R.id.txtUpSesiMatkul);
        final EditText txtUpSks = (EditText)findViewById(R.id.txtUpSksMatkul);
        Button btnUpSimpanMatkul = (Button)findViewById(R.id.btnUpSimpanMatkul);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        Intent data = getIntent();
        if(data != null){
            /*txtCariNim.setText(data.getStringExtra("nim"));
            txtUpNama.setText(data.getStringExtra("nama"));
            txtUpNim.setText(data.getStringExtra("nim"));
            txtUpEmail.setText(data.getStringExtra("email"));
            txtUpAlamat.setText(data.getStringExtra("alamat"));*/
            txtCariKode.setText(data.getStringExtra("kode"));
            txtUpNama.setText(data.getStringExtra("nama"));
            txtUpKode.setText(data.getStringExtra("kode_update"));
            txtUpHari.setText((data.getStringExtra("hari")));
            txtUpSesi.setText(data.getStringExtra("sesi"));
            txtUpSks.setText(data.getStringExtra("sks"));

            btnUpSimpanMatkul.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Mohon menunggu");
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> up = service.delete_matkul(
                            txtCariKode.getText().toString(),
                            "72180182"
                    );
                    up.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            Toast.makeText(MatkulUpdateActivity.this,"Update Success",Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MatkulUpdateActivity.this,"Failed",Toast.LENGTH_LONG).show();
                        }
                    });

                    Call<DefaultResult> addmatkul = service.add_matkul(
                            txtUpNama.getText().toString(),
                            txtUpKode.getText().toString(),
                            txtUpHari.getText().toString(),
                            txtUpSesi.getText().toString(),
                            txtUpSks.getText().toString(),
                            "72180182"
                    );

                    addmatkul.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(MatkulUpdateActivity.this,"Update Success",Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MatkulUpdateActivity.this,"Failed",Toast.LENGTH_LONG);
                        }
                    });
                }
            });
        }
    }
}