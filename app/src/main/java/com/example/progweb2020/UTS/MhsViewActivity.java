package com.example.progweb2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.progweb2020.R;

public class MhsViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mhs_view);

        TextView tvNim = (TextView)findViewById(R.id.tvNimMhsView);
        TextView tvNama = (TextView)findViewById(R.id.tvNamaMhsView);
        TextView tvEmail= (TextView)findViewById(R.id.tvEmailMhsView);
        TextView tvAlamat = (TextView)findViewById(R.id.tvAlamatMhsView);

        Intent data = getIntent();
        if(data != null){
            tvNim.setText(data.getStringExtra("nim"));
            tvNama.setText(data.getStringExtra("nama"));
            tvEmail.setText(data.getStringExtra("email"));
            tvAlamat.setText(data.getStringExtra("alamat"));
        }
    }
}