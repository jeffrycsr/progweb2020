package com.example.progweb2020.UTS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.progweb2020.Adapter.DsnAdapterUts;
import com.example.progweb2020.Model.Dosen;
import com.example.progweb2020.Network.GetDataService;
import com.example.progweb2020.Network.RetrofitClientInstance;
import com.example.progweb2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenUtsActivity extends AppCompatActivity {
    RecyclerView rvDosen;
    DsnAdapterUts dsnAdapterUts;
    ProgressDialog pd;
    List<Dosen> dosenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_uts);

        rvDosen = (RecyclerView)findViewById(R.id.rvDsnUts);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon menunggu");
        pd.show();
        androidx.appcompat.widget.Toolbar tbDsnUts = (androidx.appcompat.widget.Toolbar) findViewById(R.id.tbDosenUts);
        setSupportActionBar(tbDsnUts);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180182");

        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                dsnAdapterUts = new DsnAdapterUts(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DosenUtsActivity.this);
                rvDosen.setLayoutManager(layoutManager);
                rvDosen.setAdapter(dsnAdapterUts);
            }

            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                Toast.makeText(DosenUtsActivity.this,"Error",Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tbAdd:
            Intent intent = new Intent(DosenUtsActivity.this, DsnAddUtsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}