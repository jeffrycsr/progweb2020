package com.example.progweb2020.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Matkul {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("kode")
    @Expose
    private String kode;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("hari")
    @Expose
    private int hari;

    @SerializedName("sesi")
    @Expose
    private int sesi;

    @SerializedName("sks")
    @Expose
    private int sks;

    @SerializedName("nim_progmob")
    @Expose
    private String nim_progmob;

    public Matkul(String id, String nama, String kode, int hari, int sesi, int sks, String nim_progmob) {
        this.id = id;
        this.kode = kode;
        this.nama = nama;
        this.hari = hari;
        this.sesi = sesi;
        this.sks = sks;
        this.nim_progmob = nim_progmob;
    }

    public Matkul(String kode, String nama, int hari, int sesi, int sks, String nim_progmob) {
        this.kode = kode;
        this.nama = nama;
        this.hari = hari;
        this.sesi = sesi;
        this.sks = sks;
        this.nim_progmob = nim_progmob;
    }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getKode() { return kode; }
    public void setKode(String kode) { this.kode = kode; }

    public String getNama() { return nama; }
    public void setNama(String nama) { this.nama = nama; }

    public String getHari() { return String.valueOf(hari); }
    public void setHari(int hari) { this.hari = hari; }

    public String getSesi() { return String.valueOf(sesi); }
    public void setSesi(int sesi) { this.sesi = sesi; }

    public String getSks() { return String.valueOf(sks); }
    public void setSks(int sks) { this.sks = sks; }

    public String getNim_progmob() { return nim_progmob; }
    public void setNim_progmob(String nim_progmob) { this.nim_progmob = nim_progmob; }
}
