package com.example.progweb2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import com.example.progweb2020.Adapter.MahasiswaCardAdapter;
import com.example.progweb2020.Adapter.MahasiswaRecyclerAdapter;
import com.example.progweb2020.Model.Mahasiswa;
import com.example.progweb2020.R;
import java.util.ArrayList;
import java.util.List;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_test);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaCardAdapter mahasiswaCardAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Jeffry","72180182","087836563686");
        Mahasiswa m2 = new Mahasiswa("Robi","72180183","081234567890");
        Mahasiswa m3 = new Mahasiswa("Roni","72180182","081234567891");
        Mahasiswa m4 = new Mahasiswa("Toni","72180182","081234567892");
        Mahasiswa m5 = new Mahasiswa("Tobi","72180182","081234567893");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaCardAdapter = new MahasiswaCardAdapter(CardViewTestActivity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(CardViewTestActivity.this));
        rv.setAdapter(mahasiswaCardAdapter);
    }
}